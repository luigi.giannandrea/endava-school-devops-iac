# Endava - School of Devops Colombia
# Infrastructure as Code
# Luigi Giannandrea
#
# Configure the AWS provider
terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

# Selecting the region to work with
provider "aws" {
  region = "us-east-1"
}

# Creating the Virtual Private Cloud called esod-vpc01
resource "aws_vpc" "esod-vpc01" {
  cidr_block = "10.0.0.0/24"

  tags = {
    Name        = "esod-vpc01"
    environment = "IaC-Homework"
  }
}

# Creating the subnet esod-subnet01 and linking it to the previously created 
resource "aws_subnet" "esod-subnet01" {
  vpc_id            = aws_vpc.esod-vpc01.id
  cidr_block        = "10.0.0.0/24"
  availability_zone = "us-east-1a"

  tags = {
    Name        = "esod-subnet01"
    environment = "IaC-Homework"
  }
}

# Creating the Security Group with access to TCP Ports 80 and 22
resource "aws_security_group" "esod-sg01" {
  name        = "esod-sg01"
  description = "Allow HTTP and SSH connections"
  vpc_id      = aws_vpc.esod-vpc01.id

  # Rule to accept HTTP connections to TCP port 80 (Web application)
  ingress {
    description = "HTTP Connections"
    from_port   = 80
    to_port     = 80
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Rule to accept SSH connections to TCP port 22
  ingress {
    description = "SSH Connections"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  # Rule to allow traffic to the internet
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name        = "esod-sg01"
    environment = "IaC-Homework"
  }
}


# Creating the Network Interface to be associated to the VM
resource "aws_network_interface" "esod-nic01" {
  subnet_id   = aws_subnet.esod-subnet01.id
  private_ips = ["10.0.0.8"]

  tags = {
    Name        = "esod-nic01"
    environment = "IaC-Homework"
  }
}

# Attaching the Security Group to the Network Interface
resource "aws_network_interface_sg_attachment" "sg_attachment" {
  security_group_id    = aws_security_group.esod-sg01.id
  network_interface_id = aws_instance.esod-vm01.primary_network_interface_id
}

# Creating a Public IP Address to be used by the Network Interface and associated with the Private IP previously defined.
resource "aws_eip" "esod-ip01" {
  vpc                       = true
  network_interface         = aws_network_interface.esod-nic01.id
  associate_with_private_ip = "10.0.0.8"
}

# Creating the association between the created Public IP and the Virtual Machine
resource "aws_eip_association" "esod-ipasoc01" {
  instance_id   = aws_instance.esod-vm01.id
  allocation_id = aws_eip.esod-ip01.id
}

# Creating an Internet Gateway to allow traffic to the internet
resource "aws_internet_gateway" "esod-inetgw01" {
  vpc_id = aws_vpc.esod-vpc01.id

  tags = {
    Name = "esod-inetgw01"
  }
}

# Creating a custom Route Table to route traffic from VM to the Internet Gateway
resource "aws_route_table" "esod-rt01" {
  vpc_id = aws_vpc.esod-vpc01.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.esod-inetgw01.id
  }

  tags = {
    Name = "esod-default-rt"
  }
}

# Adding the recently created Route Table to the Subnet esod-subnet01
resource "aws_route_table_association" "esod-rtasoc-01" {
  subnet_id      = aws_subnet.esod-subnet01.id
  route_table_id = aws_route_table.esod-rt01.id
}

# Creating the Virtual Machine using Ubuntu AMI and deploying the web application.
resource "aws_instance" "esod-vm01" {
  depends_on = [
    aws_security_group.esod-sg01
  ]
  ami           = "ami-08c40ec9ead489470"
  instance_type = "t3.micro"

  network_interface {
    network_interface_id = aws_network_interface.esod-nic01.id
    device_index         = 0
  }

  credit_specification {
    cpu_credits = "unlimited"
  }
  tags = {
    Name        = "esod-vm01"
    environment = "IaC-Homework"
  }
  # Adding the user data definition to authorize the SSH Key and then install all the prerequisites to deploy the web app
  user_data = <<EOF
#!/bin/bash
echo -e "#ubuntu\nssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQC+mDyAsb62v4a8f9njUh5fTyOnu7NowpTBLMQIAM5ktlZmj8Paaw1y4sXgy2fo6MKolfpE61CZPmLHnEOTyzyt/YpyEOHYoxvSNPl1GZbWEqnDmfSd+lnLzy1cO6Upe0L1Ik/8dfvZsJMHxcHY9abzC3SfJ8ikvz/BDGD2V/hPltPysAY20p9zwGcL62elBa/Vm2dovla40L1YW0HJkat1Xy/wAArWnXYt/o5PVv4j6fNkAqu6BDCTPJ5BKUU+3/wQ4Wai6Kvkd3/97YanbNgGj260xbiIFPspHwXC9V1FdSKxSwkQ9vsZDbQVQ4SNPDtPe3CGIKOOy4D70tya0+NfMHK4QS6wh4kd3p+AXzNpgo9Rh3dXG8x4/kYI8ZQFeq4cXfnd2xwVpcQYnvKOedHIgClIBDlFHta/0xHsJO6dtYxjVpF+FmwkSc4T58VdWorPviKM7xOtu/jZNIZzikYOwDr3uBqmFGh96O/eXjIptgW8hlmJuBydlmfDbmaGKxs= luigi@tortoise" >> /home/ubuntu/.ssh/authorized_keys
sudo apt update
sudo apt install git docker.io -y
git clone https://github.com/Nexpeque/cicdworkshop
sudo sed -i 's/npm build/npm run build/g' /cicdworkshop/Dockerfile
sudo docker build -t test:v1 -f /cicdworkshop/Dockerfile /cicdworkshop
sudo docker run -d -p 80:80 --name esod-app01 test:v1
EOF
}