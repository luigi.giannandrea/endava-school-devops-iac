# Endava School of DevOps - Infrastructure as Code

## Terraform (Azure / AWS)

This repository contains the required files for the IaC Assignment.

Two folders:

- 01 Azure: main.tf file and extras
- 02 AWS: main.tf file and extras

Author: Luigi Giannandrea
