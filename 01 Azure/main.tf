# Endava - School of Devops Colombia
# Infrastructure as Code
# Luigi Giannandrea
#
# Configure the Azure provider
terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~> 3.0.2"
    }
  }
  required_version = ">= 1.1.0"
}

provider "azurerm" {
  features {}
}

# Creating the object template to hold the commands to install the web app after VM's creation
data "template_file" "script" {
  template = file("script.yml")
}

data "template_cloudinit_config" "config" {
  gzip          = true
  base64_encode = true

  part {
    content_type = "text/cloud-config"
    content      = data.template_file.script.rendered
  }
}

# Creating the resource group esod-rg01 in West Europe region (cheaper)
resource "azurerm_resource_group" "esod-rg01" {
  name     = "esod-rg01"
  location = "westeurope"
}

# Creating the Network Security Group esod-sg01
# This security group will allow connections to TCP Port 80 and TCP Port 22
resource "azurerm_network_security_group" "esod-sg01" {
  name                = "esod-sg01"
  location            = azurerm_resource_group.esod-rg01.location
  resource_group_name = azurerm_resource_group.esod-rg01.name

  security_rule {
    name                       = "allow-http"
    description                = "allow-http"
    priority                   = 100
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "80"
    source_address_prefix      = "Internet"
    destination_address_prefix = "*"
  }

  security_rule {
    name                       = "allow-ssh"
    description                = "allow-ssh"
    priority                   = 110
    direction                  = "Inbound"
    access                     = "Allow"
    protocol                   = "Tcp"
    source_port_range          = "*"
    destination_port_range     = "22"
    source_address_prefix      = "Internet"
    destination_address_prefix = "*"
  }
}

# Creating the Virtual Network esod-vnet01 and adding the dns 8.8.8.8
resource "azurerm_virtual_network" "esod-vnet01" {
  name                = "esod-vnet01"
  location            = azurerm_resource_group.esod-rg01.location
  resource_group_name = azurerm_resource_group.esod-rg01.name
  address_space       = ["10.0.0.0/16"]
  dns_servers         = ["10.0.0.4", "8.8.8.8"]

  tags = {
    environment = "Homework-IaC"
  }
}

# Creating the subnet esod-subnet01 to be used for our virtual machine
resource "azurerm_subnet" "esod-subnet01" {
  name                 = "esod-subnet01"
  resource_group_name  = azurerm_resource_group.esod-rg01.name
  virtual_network_name = azurerm_virtual_network.esod-vnet01.name
  address_prefixes     = ["10.0.1.0/24"]
}

# Linking the previously created Security Group to the Subnet
resource "azurerm_subnet_network_security_group_association" "esod-sgvnetasoc01" {
  depends_on = [azurerm_network_security_group.esod-sg01]

  subnet_id                 = azurerm_subnet.esod-subnet01.id
  network_security_group_id = azurerm_network_security_group.esod-sg01.id
}

# Creating a Public IP Configuration esod-ip01 to be used by our virtual machine and access the application
resource "azurerm_public_ip" "esod-ip01" {
  name                = "esod-ip01"
  location            = azurerm_resource_group.esod-rg01.location
  resource_group_name = azurerm_resource_group.esod-rg01.name
  allocation_method   = "Static"

  tags = {
    environment = "Homework-IaC"
  }
}

# Creating the Network Interface and adding the Public IP as a dependency
resource "azurerm_network_interface" "esod-nic01" {
  depends_on = [azurerm_public_ip.esod-ip01]

  name                = "esod-nic01"
  location            = azurerm_resource_group.esod-rg01.location
  resource_group_name = azurerm_resource_group.esod-rg01.name

  ip_configuration {
    name                          = "esod-ipconf01"
    subnet_id                     = azurerm_subnet.esod-subnet01.id
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = azurerm_public_ip.esod-ip01.id
  }
}

# Creating the Virtual Machine esod-vm01. Using a Ubuntu 20_04 instance with type Standard_B1s (1GB RAM + 1CPU)
resource "azurerm_virtual_machine" "esod-vm01" {
  name                  = "esod-vm01"
  location              = azurerm_resource_group.esod-rg01.location
  resource_group_name   = azurerm_resource_group.esod-rg01.name
  network_interface_ids = [azurerm_network_interface.esod-nic01.id]
  vm_size               = "Standard_B1s"

  # Automatically delete the Operating System disk when the VM is being deleted.
  delete_os_disk_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "0001-com-ubuntu-server-focal"
    sku       = "20_04-lts-gen2"
    version   = "latest"
  }
  storage_os_disk {
    name              = "esod-myosdisk1"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }
  os_profile {
    computer_name  = "esod-vm01"
    admin_username = "esodadmin"
    admin_password = "esodpasswd1234!"

    # Passing the commands previously defined to install git, docker, build the image and deploy the web app.
    custom_data = data.template_cloudinit_config.config.rendered
  }
  os_profile_linux_config {
    disable_password_authentication = false
  }
  tags = {
    environment = "Homework-IaC"
  }
}


